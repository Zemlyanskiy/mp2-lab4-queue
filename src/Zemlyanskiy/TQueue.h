#ifndef __TQUEUE_H__
#define __TQUEUE_H__

#include<iostream>
#include "C:\mp2-lab4-queue\include\tdataroot.h"

using namespace std;

class TQueue :public TDataRoot
{
private:
	int Hi, Li;
public:
	TQueue(int Size = DefMemSize) : TDataRoot(Size), Li(0), Hi(-1) {}

	void Put(const TData &Val)
	{
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsFull())
			SetRetCode(DataFull);
		else
		{
			if (Hi == MemSize - 1)	
				Hi = 0;
			else	
				Hi++;
			pMem[Hi] = Val;
			DataCount++;
		}
	}

	TData Get(void)
	{
		TData result = -1;
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsEmpty())
			SetRetCode(DataEmpty);
		else
		{
			if (Li == MemSize - 1)  
				Li = 0;
			else	Li++;

			result = pMem[Li];
			DataCount--;
		}
		return result;
	}

	void Print()
	{
		if (IsEmpty())
			cout << "Queue is empty";
		else if (Hi > Li)
			for (int i = Li; i <= Hi; i++)
				cout << pMem[i] << ' ';
		else {
			for (int i = Li; i < MemSize - 1; i++)
				cout << pMem[i] << ' ';
			for (int i = 0; i <= Hi; i++)
				cout << pMem[i] << ' ';
		}
		cout << endl;
	}

	int IsValid()
	{
		return 1;
	}
};

#endif 
